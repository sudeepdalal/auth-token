class User < ActiveRecord::Base
attr_accessor :password
before_save :password_encrypt
#validation need to follow https://www.owasp.org/index.php/Authentication_Cheat_Sheet PENDING 
validates_presence_of :email, :password,:password_confirmation,message:"can't be blank"
validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
validates_confirmation_of :password, message:"password and password confirmation do not match "
validates_uniqueness_of :email,message:"this email already exist"

#Password Hashed and salted  Can use has_secure_password for rails 4.1 
def password_encrypt
    if password.present?
      self.password_salt=BCrypt::Engine.generate_salt
      self.password_hash=BCrypt::Engine.hash_secret(password, password_salt)
    end
end
 def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
 end

#self.id  can be recovered from hashed and signed token on subsequent request,can be used in subsequent request
 def generate_auth_token
    payload = { user_id: self.id }
      AuthToken.encode(payload)
 end
   
end
