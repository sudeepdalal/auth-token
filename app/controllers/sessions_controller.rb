class SessionsController < ApplicationController
    helper_method :set_current_user
     skip_before_action :authenticate_request # this will be implemented later

  def new

  end
def create
  user = User.authenticate(params[:email], params[:password])
  if user
   render json: { auth_token: user.generate_auth_token }
  else
    render json: { error: 'Invalid username or password' }, status: :unauthorized
  end
end
  def destroy
    session[:user_id] = nil
    redirect_to root_url, :notice => "Logged out!"
  end
end
