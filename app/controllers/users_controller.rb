class UsersController < ApplicationController
  helper_method :set_current_user
  skip_before_action :authenticate_request 
  def new
      @user=User.new
  end
  def create
      @user=User.new(user_params)
      if @user.save
          redirect_to root_url, notice:"Signed up"
      else
          render "new"
      end
  end
 
  private
  def user_params
      params.require(:user).permit(:email,:password, :password_confirmation)
  end
end
